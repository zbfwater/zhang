package z

import (
	"gitee.com/zbfwater/zhang/zdao"
	"github.com/gogf/gf/database/gdb"
)

/**
获取 DB对象.
  默认
*/
func DB(names ...string) gdb.DB {
	return zdao.DB(names...)

}

/**
获取 DB对象.
  name : 配置名称,默认null
*/
func GetDB(name string) gdb.DB {
	return DB(name)
}
