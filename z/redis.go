package z

import (
	"gitee.com/zbfwater/zhang/zredis"
)

func Redis(name ...string) *zredis.Redis {
	return zredis.GetRedis(name...)
}
