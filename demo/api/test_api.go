package api

import (
	"gitee.com/zbfwater/zhang/zweb"
	"github.com/gogf/gf/net/ghttp"
)

type TestApi struct {
	*zweb.ApiBase
}

func (api *TestApi) Register(server *ghttp.Server) {
	server.BindHandler("test", api.test)
}
func (api *TestApi) test(r *ghttp.Request) {
	_ = api.OkResult("test", r)
}
