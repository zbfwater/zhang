package main

import (
	"gitee.com/zbfwater/zhang"
	"gitee.com/zbfwater/zhang/z"
	"gitee.com/zbfwater/zhang/zlog"
)

func main() {
	s := zhang.Default()
	z.OpenBrowse(z.GetUrl())
	r := z.Redis("abc")
	a, err := r.Do("GET", "test")
	zlog.LogError(err, a)
	s.Run()
}
